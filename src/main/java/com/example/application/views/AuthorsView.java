package com.example.application.views;

import com.example.application.Author;
import com.example.application.MyDriverManager;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Route("authors")
public class AuthorsView extends VerticalLayout {

    public AuthorsView() {
        MyDriverManager driverManager = new MyDriverManager();
        ArrayList<Author> authors = driverManager.getAutors();
        Grid<Author> grid = new Grid<>();
//        ResultSet rs = driverManager.getAutors();

        ListBox<String> listBox = new ListBox<>();
        add(new H1("Authors"));

        grid.setItems(authors);

        grid.addColumn(Author::getJmeno).setHeader("Jmeno");
        grid.addColumn(Author::getPrijmeni).setHeader("Prijmeni");


        add(grid);
    }

    private List<String> getAuthorsFromResultSet(ResultSet rs) {
        List<String> authors = new ArrayList<>();
        try {
            while (rs.next()) {
                String author = rs.getString("JMENO") + " " + rs.getString("PRIJMENI");
//                authors.add(author);
//                System.out.println(rs.getString("JMENO"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return authors;
    }

    // Your getAutors() function here
}