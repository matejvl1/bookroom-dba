package com.example.application;

import java.sql.*;
import java.util.ArrayList;


public class MyDriverManager {

    public static void main(String[] args) {
        MyDriverManager driverManager = new MyDriverManager();
        ArrayList<Author> authors = driverManager.getAutors();


        for (Author autor : authors
             ) {
            System.out.println(autor.getJmeno() + " " + autor.getPrijmeni());
        }

    }

    private static final String url = "jdbc:oracle:thin:@ora1.uhk.cz:1521:orcl";
    private static final String user = "DBmalihvi1";
    private static final String password = "DBmalihvi122";

    public ArrayList<Author> getAutors() {
        ArrayList<Author> autori = new ArrayList();

        try (Connection conn = DriverManager.getConnection(url, user, password)) {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT JMENO, PRIJMENI, ZIVOTOPIS, AUTORIID, NARODNOST, DATUM_NAROZENI, DATUM_UMRTI FROM AUTORI");


            while (rs.next()) {
                autori.add(new Author(rs.getString("JMENO"), rs.getString("PRIJMENI")));
            }

            return (ArrayList<Author>) autori;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return autori;
        }
    }
}
